import requests, smtplib, pika, json, os
from email.message import EmailMessage
from time import sleep
from config import settings
import sentry_sdk

if settings.SENTRY_URL != "":
    sentry_sdk.init(
        settings.SENTRY_URL,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

sleep(30)

def send_mail(msg: EmailMessage)-> None:
    with smtplib.SMTP(settings.MAIL_SERVER, settings.MAIL_PORT) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(msg["From"], settings.MAIL_PASSWORD)
        smtp.send_message(msg)


def get_anthem(ch, method, properties, body):
    params = json.loads(body.decode())
    url = params["url"]
    country = params["country"]
    email = params["email"]
    ext = url.split(".")[-1]
    hdrs = {"User-Agent": "api5/1.0 (no contacts) fastapi/0.72.0"}
    resp = requests.get(url, headers=hdrs)
    mailfrom = settings.MAIL_USERNAME
    msg = EmailMessage()
    msg["Subject"] = f"Anthem of {country}"
    msg["From"] = mailfrom
    msg["To"] = email
    if resp.content[1:15] == "<!DOCTYPE html>":
        sentry_sdk.capture_message(f"Anthem of {country} downloading failed, remote service unavliable")
        return "Anthem downloading failed, remote service unavliable"
    else:
        msg.add_attachment(resp.content, filename=f"Anthem_of_{country}.{ext}", maintype="audio", subtype=ext)
        send_mail(msg)

if settings.APP_ENV != "test":
    parameters = pika.URLParameters(settings.CELERY_BROKER_URL)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="get_anthem")
    channel.basic_consume(queue='get_anthem', on_message_callback=get_anthem, auto_ack=True)
