import pytest, json
from fastapi.testclient import TestClient
from unittest.mock import patch
import app
from config import settings

correct_url = {"url":"https://upload.wikimedia.org/wikipedia/commons/4/41/National_Anthem_of_Russia_%282000%29%2C_instrumental%2C_one_verse.ogg",
               "country": "Russia",
               "email":"test@test.test"}

incorrect_url = {"url":"https://nikakih_gimnov.net",
                 "country": "Russia",
                 "email":"test@test.test"}

@pytest.fixture
def client():
    yield TestClient(app)

def test_correct_country(client):
    # Запрос гимна с полными данными
    with patch("app.send_mail",) as send_mail:
        send_mail.return_value = None
        with patch("email.message.EmailMessage.add_attachment") as send_message:
            body = str.encode(json.dumps(correct_url))
            app.get_anthem(None, None, None, body)
            called = send_message.assert_called()
            assert called == None

if __name__ == "__main__":
    pytest.main()
