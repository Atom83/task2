import schedule
import time
from app import send_mail
from config import settings

schedule.every(settings.REPORT_PERIOD).minutes.do(send_mail)
 
while True:
    schedule.run_pending()
    time.sleep(1)