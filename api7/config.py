from pydantic import BaseSettings
import os
import dotenv

dotenv.load_dotenv()

app_env = os.getenv("APP_ENV", None)

class Settings(BaseSettings):
    APP_ENV: str = ""
    PG_PASSWORD: str
    PG_USER: str
    PG_DB: str
    PG_PASSWORD_TEST: str
    PG_USER_TEST: str
    PG_DB_TEST: str
    PG_HOST: str
    PG_PORT: str
    PG_URI: str = ""
    SENTRY_URL: str
    MAIL_SERVER: str
    MAIL_PORT: int
    MAIL_USE_TLS: str
    MAIL_USERNAME: str
    MAIL_PASSWORD: str
    CELERY_BROKER_URL: str
    ADM_LST: str
    REPORT_PERIOD: int

settings = Settings(_env_file=f"{app_env}.env")
settings.APP_ENV = app_env
if app_env == "test":
    settings.PG_DB = settings.PG_DB_TEST
    settings.PG_PASSWORD = settings.PG_PASSWORD_TEST
    settings.PG_USER = settings.PG_USER_TEST
settings.PG_URI = f"postgresql://{settings.PG_USER}:{settings.PG_PASSWORD}@{settings.PG_HOST}:{settings.PG_PORT}/{settings.PG_DB}"
