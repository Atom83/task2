import smtplib, sentry_sdk
from datetime import datetime
from fpdf import FPDF, HTMLMixin
from database import get_req_db
from config import settings

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email import encoders
import base64
from time import sleep

if settings.SENTRY_URL != "":
    sentry_sdk.init(
        settings.SENTRY_URL,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

sleep(30)

class HTML2PDF(FPDF, HTMLMixin):
    pass

def make_pdf(content: str)-> bytes:
    pdf_doc = HTML2PDF()
    pdf_doc.add_page()
    pdf_doc.write_html(content)
    return pdf_doc.output("anthems_report.pdf", "S")


def send_mail():
    req_data = get_req_db()
    email_lst = settings.ADM_LST
    mailfrom = settings.MAIL_USERNAME
    pwd = settings.MAIL_PASSWORD
    msg = MIMEMultipart()
    msg["Subject"] = "Anthems requests"
    msg["From"] = mailfrom
    msg['To'] = email_lst
    if len(req_data["reqs"]) == 0:
        sentry_sdk.capture_message("No data to report")
        content = f"Requests: No data to report<br>Report sent at {datetime.now()}<br>Don't worry. Be happy! :o)"
    else:
        sentry_sdk.capture_message("Makeing a report")
        content = "Requests:<br>"
        for req in req_data["reqs"]:
            content += f"{req['email']}: {req['country']} at {req['execdt']}<br>"
        content += f"Report sent at {datetime.now()}"
    pdf_file = make_pdf(content)
    part = MIMEApplication(pdf_file, _subtype = 'pdf', _encoder = encoders.encode_base64)
    part.add_header('Content-Disposition', 'attachment', filename='anthems_report.pdf')
    msg.attach(part)
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(mailfrom, pwd)
        smtp.send_message(msg)
