import pytest, json
from fastapi.testclient import TestClient
from unittest.mock import patch
import app
from config import settings

@pytest.fixture
def client():
    yield TestClient(app)

def test_pdf(client):
    pdf = app.make_pdf("test")
    assert pdf != None

if __name__ == "__main__":
    pytest.main()
