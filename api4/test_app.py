import pytest, json
from fastapi.testclient import TestClient
from unittest.mock import patch
import app
from config import settings

correct_country = {"country": "Russia"}

incorrect_country = {"country": "Hobbiton"}

@pytest.fixture
def client():
    yield TestClient(app)

def test_correct_country(client):
    # Запрос гимна с полными данными
    with patch("app.publish") as basic_publish:
        basic_publish.return_value = None
        body = str.encode(json.dumps(correct_country))
        result = app.find_anthem(None, None, None, body)
        assert result == None

def test_wrong_country(client):
    # Запрос гимна с полными данными
    with patch("app.publish") as basic_publish:
        basic_publish.return_value = None
        body = str.encode(json.dumps(incorrect_country))
        result = app.find_anthem(None, None, None, body)
        assert result["Error"] != None

if __name__ == "__main__":
    pytest.main()
