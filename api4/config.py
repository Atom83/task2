from pydantic import BaseSettings
import os
import dotenv

dotenv.load_dotenv()

app_env = os.getenv("APP_ENV", None)

class Settings(BaseSettings):
    APP_ENV: str = ""
    SENTRY_URL: str
    MAIL_SERVER: str
    MAIL_PORT: int
    MAIL_USE_TLS: str
    MAIL_USERNAME: str
    MAIL_PASSWORD: str
    CELERY_BROKER_URL: str
    REDIS_HOSTS: str
    REDIS_PORT: int

settings = Settings()
settings.APP_ENV = app_env