import requests
from bs4 import BeautifulSoup
import pika, json
from pika.adapters.blocking_connection import BlockingChannel
from time import sleep
import redis
import sentry_sdk
from config import settings

if settings.SENTRY_URL != "":
    sentry_sdk.init(
        settings.SENTRY_URL,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

sleep(30)

def publish(pars)-> None:
    cache.mset({pars["country"]: pars["url"]})
    channel.basic_publish(exchange=''
                        , routing_key='get_anthem'
                        , body=json.dumps(pars))

def find_anthem(ch, method, properties, body):
    params = json.loads(body.decode())
    URL_COUNTRIES = "https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations)"
    URL_WIKI = "https://en.wikipedia.org"
    o_url = ""
    country = params["country"]
    resp = requests.get(URL_COUNTRIES)
    soup = BeautifulSoup(resp.text, "html.parser")
    tables = soup.find_all("table")
    for tab in tables:
        if tab.find("th").text[:-1] == "Country/Area":
            for row in tab.find("tbody").find_all("tr"):
                a = row.find("a")
                if a.text == country:
                    country_url = URL_WIKI + a.attrs["href"]
                    c_resp = requests.get(country_url)
                    c_soup = BeautifulSoup(c_resp.text, "html.parser")
                    c_table = c_soup.find("table", attrs={"class":"infobox"})
                    a = c_table.find("td", attrs={"class":"infobox-full-data anthem"})\
                                .find("a")
                    a_url = a.attrs["href"]
                    title = a.attrs["title"]
                    a_resp = requests.get(URL_WIKI+a_url)
                    a_soup = BeautifulSoup(a_resp.text, "html.parser")
                    a_table = a_soup.find("table", attrs={"class":"infobox"})
                    snd_url = a_table.find("tr", attrs={"class":"haudio"})\
                                    .find("div" ,attrs={"class":"hlist hlist-separated"})\
                                    .find("a").attrs["href"]
                    s_resp = requests.get(URL_WIKI+snd_url)
                    s_soup = BeautifulSoup(s_resp.text, "html.parser")
                    o_url = "http:"+s_soup.find("div", attrs={"class":"fullMedia"}).find("a").attrs["href"]
                    break
            break
    if o_url:
        sentry_sdk.capture_message(f"Anthem of {country} found at {o_url}")
        params["url"] = o_url
        publish(params)
    else:
        sentry_sdk.capture_message(f"{country}'s anthem's url not found")
        return {"Error": f"{country}'s anthem's url not found"}

if settings.APP_ENV != "test":
    parameters = pika.URLParameters(settings.CELERY_BROKER_URL)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="get_anthem")
    channel.queue_declare(queue="find_anthem")
    channel.basic_consume(queue='find_anthem', on_message_callback=find_anthem, auto_ack=True)

    cache = redis.Redis(host=settings.REDIS_HOSTS, port=settings.REDIS_PORT)

