from pydantic import BaseModel, Field
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, create_engine
from sqlalchemy.orm import sessionmaker
from config import settings
from datetime import datetime

DeclarativeBase = declarative_base()

class Country_reqSchema(BaseModel):
    country: str = Field(...)
    email: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "country": "Country_name",
                "email": "example@exmpl_mailer.exmpl",
            }
        }

class Country_req(DeclarativeBase):
    __tablename__ = "requests"
    id = Column(Integer, primary_key=True)
    country = Column("country", String)
    email = Column("email", String)
    execdt = Column("execdt", DateTime, default=datetime.now())

    def __repr__(self):
        return "".format(self.id)

def req_helper(req) -> dict:
    return {
        "_id": str(req.id),
        "country": req.country,
        "email": req.email,
        "execdt": req.execdt,
    }

def ResponseModel(data, message: str) -> dict:
    return {"data": [data], "code": 200, "message": message}


def ErrorResponseModel(error: str, code: int, message: str) -> dict:
    return {"error": error, "code": code, "message": message}

engine = create_engine(settings.PG_URI)
DeclarativeBase.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

async def add_req_db(req: Country_reqSchema) -> dict:
    new_req = Country_req(country=req["country"], \
                          email=req["email"])
    session.add(new_req)
    session.commit()
    return req_helper(new_req)
