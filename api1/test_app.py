import pytest
from fastapi.testclient import TestClient
from unittest.mock import patch
from app import app
from database import session, Country_req
from config import settings

full_request = {
    "country": "Russia",
    "email": "example@exmpl_mailer.exmpl"
}
just_country = {
    "country": "Russia"
}
just_mail = {
    "email": "example@exmpl_mailer.exmpl"
}

def truncate_DB():
    session.query(Country_req).delete()
    session.commit()

@pytest.fixture
def client():
    yield TestClient(app)

# Чистим тестовую базу во избежание конфликтов по данным
def test_trunc1_db(client):
    truncate_DB()

def test_half_data_request(client):
    # Запрос гимна с неполными данными: только страна,
    # без адреса email, должен корректно обработаться 
    # и вернуть в ответе ошибку
    response = client.post("/", json=just_country)
    assert response.status_code == 200
    assert response.json()["code"] == 400

def test_another_half_data_request(client):
    # Запрос гимна с неполными данными: только адрес email,
    # без страны, должен корректно обработаться
    # и вернуть в ответе ошибку
    response = client.post("/", json=just_mail)
    assert response.status_code == 200
    assert response.json()["code"] == 400

def test_full_request(client):
    # Запрос гимна с полными данными
    with patch("app.publish") as basic_publish:
        basic_publish.return_value = None
        response = client.post("/", json=just_mail)
        assert response.status_code == 200

# Убираем за собой мусор во избежание конфликтов со следующим разработчиком
def test_trunc2_db(client):
    truncate_DB()

if __name__ == "__main__":
    pytest.main()
