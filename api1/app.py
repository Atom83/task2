from fastapi import FastAPI, Body
from fastapi.encoders import jsonable_encoder
from time import sleep
import pika, json, redis, sentry_sdk
from sqlalchemy.exc import SQLAlchemyError
from config import settings
from database import add_req_db, session, ResponseModel, ErrorResponseModel

if settings.SENTRY_URL != "":
    sentry_sdk.init(
        settings.SENTRY_URL,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

app = FastAPI()

sleep(30)

if settings.APP_ENV != "test":
    parameters = pika.URLParameters(settings.CELERY_BROKER_URL)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="find_anthem")
    channel.queue_declare(queue="get_anthem")
    
    cache = redis.Redis(host=settings.REDIS_HOSTS, port=settings.REDIS_PORT)

def publish(pars:dict)-> None:
    cache_answer = cache.get(pars["country"])
    if cache_answer:
        pars["url"] = cache_answer.decode()
        channel.basic_publish(exchange=''
                            , routing_key='get_anthem'
                            , body=json.dumps(pars))
        sentry_sdk.capture_message(f"Cached anthem URL: {pars['url']}")
    else:
        channel.basic_publish(exchange=''
                            , routing_key='find_anthem'
                            , body=json.dumps(pars))
        sentry_sdk.capture_message(f"Uncached anthem URL for {pars['country']}")
    

@app.post("/", tags=["Root"])
async def root(params = Body(...)):
    params = jsonable_encoder(params)
    try:
        country = params["country"]
    except:
        return {"error": "Invalid request", "code": 400, "message": "Country not found"}
    try:
        email = params["email"]
    except:
        return {"error": "Invalid request", "code": 400, "message": "Email not found"}

    try:
        new_req = await add_req_db(params)
        sentry_sdk.capture_message(f"A new request for anthem of {country} was added successfully")
    except SQLAlchemyError as e: 
        session.rollback()
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Internal server error", 500, e.orig.args)
    except Exception as e: 
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Error", 520, e)
    
    publish(params)

    return ResponseModel(new_req, "Request registered successfully")
